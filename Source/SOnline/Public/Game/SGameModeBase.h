// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Game/SOnlineGameMode.h"
#include "SGameplayTypes.h"
#include "SGameModeBase.generated.h"

class ASPlayerStart;

UCLASS()
class SONLINE_API ASGameModeBase : public ASOnlineGameMode
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "GameSettings")
	bool AllowJoinInProgress;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "GameSettings")
	bool AutoRespawn;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "GameSettings", meta = (EditCondition = "AutoRespawn"))
	float PlayerRespawnTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "GameSettings", meta = (EditCondition = "AutoRespawn"))
	float TimeToRemoveBodies;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Limits")
	bool LimitByTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Limits", meta = (EditCondition = "LimitByTime"))
	int TimeLimitInSeconds;

	virtual void OnPlayerConnected(APlayerController* Controller) override;

	virtual void UpdateDeathPoints(APlayerController* DeadPlayer, APlayerController* DeathInstigator);

	virtual TArray<ASPlayerStart*> GetFreeSpawnPoints(APlayerController* Controller);

	void SpawnAllPlayers();

	void SpawnSpectator(APlayerController* Player, bool DeleteCurrentPawn);

	void AllPlayersToSpectators(bool DeletePawns);

public:

	ASGameModeBase();

	virtual void PlayerDead(APlayerController* DeadPlayer, APlayerController* DeathInstigator, AActor* DeathCauser);

	virtual void SpawnPlayer(APlayerController* Controller);

	virtual void StartGame();

	virtual void StopGame(FString Reason);

	virtual void EndGame();

	void TimeLimitReached();

	virtual bool ShouldDamagePlayer(APlayerController* DamagedPlayer, APlayerController* DamageInstigator);

	bool GetLimitByTime();
	int GetTimeLimit();
};
