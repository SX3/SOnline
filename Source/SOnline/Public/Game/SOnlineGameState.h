// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "SGameplayTypes.h"
#include "SOnlineGameState.generated.h"

class ASPlayerState;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPlayerConnectedSignature, APlayerController*, Controller);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FGameStateChangeSignature, EGameState, NewState);

UCLASS()
class SONLINE_API ASOnlineGameState : public AGameStateBase
{
	GENERATED_BODY()

protected:

	FTimerHandle StateTimeTick;

	virtual void BeginPlay() override;

	virtual void GameStateTick();

	virtual bool EnoughPlayersToStart();

	virtual void GameLogicTick();

	bool AllPlayersReady();
	
public:
	ASOnlineGameState();
	
	UPROPERTY(Replicated, BlueprintReadOnly)
	EGameState GameState;

	UPROPERTY(BlueprintAssignable)
	FPlayerConnectedSignature PlayerConnected;

	UPROPERTY(BlueprintAssignable)
	FGameStateChangeSignature NewGameState;

	UFUNCTION(NetMulticast,Reliable)
	void SetNewGameState(EGameState NewState);

	UFUNCTION()
	virtual void NewPlayerConnected(APlayerController* Controller);

	virtual void PlayerReady(APlayerState* PlayerState);

	UFUNCTION(BlueprintPure)
	TArray<ASPlayerState*> GetPlayers();
};
