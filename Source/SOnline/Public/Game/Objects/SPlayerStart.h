// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerStart.h"
#include "SGameplayTypes.h"
#include "SPlayerStart.generated.h"

class ASOnlineCharacter;

UCLASS()
class SONLINE_API ASPlayerStart : public APlayerStart
{
	GENERATED_BODY()
	

	UFUNCTION()
	void StartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

	UFUNCTION()
	void EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

protected:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "PlayerStartSettings")
	ETeam PointTeam;

	bool IsFreePoint;

	virtual void BeginPlay() override;

public:

	ASOnlineCharacter* SpawnCharacter(APlayerController* Controller);

	bool IsFree();

};
