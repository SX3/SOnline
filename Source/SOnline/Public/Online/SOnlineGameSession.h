// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameSession.h"
#include "SOnlineTypes.h"
#include "SOnlineGameSession.generated.h"


UCLASS()
class SONLINE_API ASOnlineGameSession : public AGameSession
{
	GENERATED_BODY()

protected:

	ASOnlineGameSession();

	virtual void BeginPlay() override;

	virtual void RegisterServer() override;

};
