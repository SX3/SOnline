// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "SOnlineTypes.h"

#include "FindSessionsCallbackProxy.h" // FBlueprintSessionResult

#include "SOnlineSessionsLibrary.generated.h"


#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4996)
// #TODO check back on this at some point
#pragma warning(disable:4265) // SteamAPI CCallback< specifically, this warning is off by default but 4.17 turned it on....
#endif

#if PLATFORM_WINDOWS || PLATFORM_MAC || PLATFORM_LINUX

#pragma push_macro("ARRAY_COUNT")
#undef ARRAY_COUNT

#if USING_CODE_ANALYSIS
MSVC_PRAGMA(warning(push))
MSVC_PRAGMA(warning(disable : ALL_CODE_ANALYSIS_WARNINGS))
#endif	// USING_CODE_ANALYSIS

#include <steam/steam_api.h>

#if USING_CODE_ANALYSIS
MSVC_PRAGMA(warning(pop))
#endif	// USING_CODE_ANALYSIS

#include <steam/isteamapps.h>
#include <steam/isteamapplist.h>
//#include <OnlineSubsystemSteamTypes.h>
#pragma pop_macro("ARRAY_COUNT")

// @todo Steam: See above
#ifdef _MSC_VER
#pragma warning(pop)
#endif
#endif


UCLASS()
class SONLINE_API USOnlineSessionsLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	UFUNCTION(BlueprintPure, Category = "Online|PlayerInfo")
		static FSUniqueNetId GetUniqueNetID(APlayerController *PlayerController);

	UFUNCTION(BlueprintPure, Category = "Online|PlayerInfo")
		static FSUniqueNetId GetUniqueNetIDByPlayerState(APlayerState* PlayerState);

	// Get a friend from the previously read/saved friends list (Must Call GetFriends first for this to return anything)
	UFUNCTION(BlueprintCallable, Category = "Online|PlayerInfo")
		static FSFriendInfo GetFriend(APlayerController* PlayerController,const FSUniqueNetId FriendID);

	UFUNCTION(BlueprintCallable, Category = "Online|SessionInfo")
		static void GetExtraSettings(FBlueprintSessionResult SessionResult, TArray<FSSessionPropertyKeyPair> & ExtraSettings);

	UFUNCTION(BlueprintPure, Category = "Online|Identity")
		static FString GetPlayerNickname(const FSUniqueNetId &UniqueNetID);



	// Get session custom information key/value as Byte (For Enums)
	UFUNCTION(BlueprintCallable, Category = "Online|SessionInfo", meta = (ExpandEnumAsExecs = "SearchResult"))
		static void GetSessionPropertyByte(const TArray<FSSessionPropertyKeyPair> & ExtraSettings, FName SettingName, ESessionSettingSearchResult &SearchResult, uint8 &SettingValue);

	// Get session custom information key/value as Bool
	UFUNCTION(BlueprintCallable, Category = "Online|SessionInfo", meta = (ExpandEnumAsExecs = "SearchResult"))
		static void GetSessionPropertyBool(const TArray<FSSessionPropertyKeyPair> & ExtraSettings, FName SettingName, ESessionSettingSearchResult &SearchResult, bool &SettingValue);

	// Get session custom information key/value as String
	UFUNCTION(BlueprintCallable, Category = "Online|SessionInfo", meta = (ExpandEnumAsExecs = "SearchResult"))
		static void GetSessionPropertyString(const TArray<FSSessionPropertyKeyPair> & ExtraSettings, FName SettingName, ESessionSettingSearchResult &SearchResult, FString &SettingValue);

	// Get session custom information key/value as Int
	UFUNCTION(BlueprintCallable, Category = "Online|SessionInfo", meta = (ExpandEnumAsExecs = "SearchResult"))
		static void GetSessionPropertyInt(const TArray<FSSessionPropertyKeyPair> & ExtraSettings, FName SettingName, ESessionSettingSearchResult &SearchResult, int32 &SettingValue);

	// Get session custom information key/value as Float
	UFUNCTION(BlueprintCallable, Category = "Online|SessionInfo", meta = (ExpandEnumAsExecs = "SearchResult"))
		static void GetSessionPropertyFloat(const TArray<FSSessionPropertyKeyPair> & ExtraSettings, FName SettingName, ESessionSettingSearchResult &SearchResult, float &SettingValue);

	// Make a literal session custom information key/value pair from Byte (For Enums)
	UFUNCTION(BlueprintPure, Category = "Online|SessionInfo|Literals")
		static FSSessionPropertyKeyPair MakeLiteralSessionPropertyByte(FName Key, uint8 Value);

	// Make a literal session custom information key/value pair from Bool
	UFUNCTION(BlueprintPure, Category = "Online|SessionInfo|Literals")
		static FSSessionPropertyKeyPair MakeLiteralSessionPropertyBool(FName Key, bool Value);

	// Make a literal session custom information key/value pair from String
	UFUNCTION(BlueprintPure, Category = "Online|SessionInfo|Literals")
		static FSSessionPropertyKeyPair MakeLiteralSessionPropertyString(FName Key, FString Value);

	// Make a literal session custom information key/value pair from Int
	UFUNCTION(BlueprintPure, Category = "Online|SessionInfo|Literals")
		static FSSessionPropertyKeyPair MakeLiteralSessionPropertyInt(FName Key, int32 Value);

	// Make a literal session custom information key/value pair from Float
	UFUNCTION(BlueprintPure, Category = "Online|SessionInfo|Literals")
		static FSSessionPropertyKeyPair MakeLiteralSessionPropertyFloat(FName Key, float Value);


	// ===== STEAM ===== //

	UFUNCTION(BlueprintCallable, Category = "Online|SteamAPI", meta = (ExpandEnumAsExecs = "Result"))
		static UTexture2D * GetSteamFriendAvatar(const FSUniqueNetId UniqueNetId, EBlueprintAsyncResultSwitch &Result, ESteamAvatarSize AvatarSize);
	
};
