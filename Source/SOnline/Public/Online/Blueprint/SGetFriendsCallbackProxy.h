// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Online.h"
#include "SOnlineTypes.h"
#include "Runtime/Engine/Public/Net/OnlineBlueprintCallProxyBase.h"
#include "SGetFriendsCallbackProxy.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBlueprintGetFriendsListDelegate, const TArray<FSFriendInfo>&, Results);

UCLASS()
class SONLINE_API USGetFriendsCallbackProxy : public UOnlineBlueprintCallProxyBase
{
	GENERATED_UCLASS_BODY()

public:

	UPROPERTY(BlueprintAssignable)
		FBlueprintGetFriendsListDelegate OnSuccess;

	// Called when there was an error retrieving the friends list
	UPROPERTY(BlueprintAssignable)
		FBlueprintGetFriendsListDelegate OnFailure;

	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"), Category = "Online|SSubSystem")
		static USGetFriendsCallbackProxy* SGetFriendsList(UObject* WorldContextObject, class APlayerController* PlayerController);

	virtual void Activate() override;

	

private:
	// Internal callback when the friends list is retrieved
	void OnReadFriendsListCompleted(int32 LocalUserNum, bool bWasSuccessful, const FString& ListName, const FString& ErrorString);

	// The player controller triggering things
	TWeakObjectPtr<APlayerController> PlayerControllerWeakPtr;

	// The delegate executed
	FOnReadFriendsListComplete FriendListReadCompleteDelegate;

	// The world context object in which this call is taking place
	UObject* WorldContextObject;


};
