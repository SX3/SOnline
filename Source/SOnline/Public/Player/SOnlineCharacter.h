// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SOnlineCharacter.generated.h"

class USHealthComponent;
class USWeaponManager;
class USBodySystemsComponet;

UCLASS(config=Game)
class ASOnlineCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

protected:

	void MoveForward(float Value);

	void MoveRight(float Value);

	void TurnAtRate(float Rate);

	void LookUpAtRate(float Rate);

	void StartCrouch();

	void StopCrouch();

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	virtual void BeginPlay() override;

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerTraceInterract();

	void StartUseWeapon();
	void StopUseWeapon();

	void StartSprint();
	void StopSprint();

	void SelectBasicSlot();
	void SelectSecondarySlot();
	void SelectAdditionalSlot();

public:

	ASOnlineCharacter();

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly)
	USWeaponManager* WeaponManager;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly)
	USBodySystemsComponet* BodySystems;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Animation") //����  ������� ������� AimOffset
	FRotator LookAngles;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

	virtual FVector GetPawnViewLocation() const override; //��������������� ����� ������ ��������(�� ������)

	FVector GetCameraForwardVector();

	FRotator GetCameraRotation();

	virtual void Tick(float DeltaTime) override;

	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};

