// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SOnlineTypes.h"
#include "SWeaponManager.generated.h"

class ASWeaponBase;
class ASOnlineCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPlayerWeaponChange, ESlotType, NewSlot);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SONLINE_API USWeaponManager : public UActorComponent
{
	GENERATED_BODY()

protected:

	ASOnlineCharacter* OwnerCharacter;

	UPROPERTY(Replicated, BlueprintReadOnly)
		ASWeaponBase* Slot_Basic;
	UPROPERTY(Replicated, BlueprintReadOnly)
		ASWeaponBase* Slot_Secondary;
	UPROPERTY(Replicated, BlueprintReadOnly)
		ASWeaponBase* Slot_Additional;
	UPROPERTY(Replicated, BlueprintReadOnly)
		TArray<ASWeaponBase*> Slot_Belt;

	virtual void BeginPlay() override;

	void WeaponAttach(ASWeaponBase* Weapon);

	bool AbleToAddWeapon(ASWeaponBase* Weapon);
	bool AbleToAddBelt(ASWeaponBase* Weapon);

public:	

	USWeaponManager();

	UPROPERTY(Replicated, BlueprintReadOnly)
	ESlotType CurrentSlot;

	ESlotType AttachedSlot;

	UPROPERTY(BlueprintAssignable)
	FOnPlayerWeaponChange OnChangeWeapon;

	UFUNCTION(BlueprintCallable, NetMulticast, Reliable)
	void AddWeapon(ASWeaponBase* Weapon);

	UFUNCTION(Server, Reliable, WithValidation)
	void SelectWeaponToServer(ESlotType SelectSlot);

	bool SelectWeapon(ESlotType SelectSlot);

	UFUNCTION(NetMulticast, Reliable)
	void ChangedWeapon(ESlotType NewSlot);
	
	UFUNCTION(BlueprintPure)
	ASWeaponBase* GetCurrentWeapon();

	ASWeaponBase* GetWeaponFromSlot(ESlotType SlotToGet);
};
