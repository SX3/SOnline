// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "SDamageType.generated.h"

UCLASS()
class SONLINE_API USDamageType : public UDamageType
{
	GENERATED_BODY()

public:

	EPhysicalSurface DamageBySurface;
};
