// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Global/SBaseGlobalActor.h"
#include "SGameplayTypes.h"
#include "SOnline.h"
#include "SWeaponBase.generated.h"


#define SWEAPON_DEBUG

///// SURFACE TYPES /////



///// DEFINE SYRFACE END /////

class ASOnlineCharacter;

UCLASS()
class SONLINE_API ASWeaponBase : public ASBaseGlobalActor
{
	GENERATED_BODY()
	
protected:

	ASOnlineCharacter* WeaponOwner;

	UPROPERTY(VisibleAnywhere, Category = "VisualSettings")
	USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponSettings")
	FName WeaponName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponSettings")
	ESlotType SlotType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponSettings")
	EWeaponType WeaponType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponSettings")
	float WeaponDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponSettings")
	bool AutoUse;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponSettings")
	float UseRate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponSettings")
	TSubclassOf<UDamageType> DamageType;

	UPROPERTY(EditDefaultsOnly, Category = "VisualSettings")
	TSubclassOf<UCameraShake> UseCameraShake;

	FTimerHandle UseCooldownTimer;

	float LastUsedTime;
	float TimeBetweenUsed;

	virtual void BeginPlay() override;

	virtual void Interact(ASOnlineCharacter* InteractPlayer) override;

				///// USE WEAPON /////
public:

	void UseWeapon(bool Start_Stop); // Global Start Use Weapon

protected:

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerStartUseWeapon(); // Send to server Start Use Weapon

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerStopUseWeapon(); // Send to server Stop Use Weapon

	UFUNCTION(NetMulticast, Reliable)
	void ClientStartUseWeapon(); // Send to clients signal about started use weapon

	UFUNCTION(NetMulticast, Reliable)
	void ClientStopUseWeapon(); // Send to clients signal about stoped use weapon

	UFUNCTION()
	virtual void UseWeaponLogic(); // Logic use weapon on clients

public:

	ASWeaponBase();

	FCollisionQueryParams WeaponCollision;

	virtual void ChangeWeaponAction();

		///// GETTERS /////

	USkeletalMeshComponent* GetMesh();

	ESlotType GetSlotType();

	///// SETTERS /////

	void SetWeaponOwner(ASOnlineCharacter* NewOwner);
};
