// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Online.h"
#include "SOnline.h"
#include "SOnlineTypes.h"
#include "OnlineSubsystem.h"
#include "FindSessionsCallbackProxy.h"

#include "SGameInstance.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FFindSessionsCompleteEvent, const TArray<FBlueprintSessionResult>&, SessionsResult);

UCLASS()
class SONLINE_API USGameInstance : public UGameInstance
{
	GENERATED_BODY()

	/* ������� ���������� ��� �������� ������ */
	FOnCreateSessionCompleteDelegate OnCreateSessionCompleteDelegate;
	/* ������� ���������� ��� ������ ������ */
	FOnStartSessionCompleteDelegate OnStartSessionCompleteDelegate;
	/* ������� ��� ������ ������ */
	FOnFindSessionsCompleteDelegate OnFindSessionsCompleteDelegate;

	/* ����������� ������������������ ��������� ��� �������� / ������ ������ */
	FDelegateHandle OnCreateSessionCompleteDelegateHandle;
	FDelegateHandle OnStartSessionCompleteDelegateHandle;
	/* ���������� ������������������� �������� ��� ������ ������ */
	FDelegateHandle OnFindSessionsCompleteDelegateHandle;

	/* ������� ��� ����������� ������ */
	FOnDestroySessionCompleteDelegate OnDestroySessionCompleteDelegate;
	/* ���������� ������������������� �������� ��� ����������� ������ */
	FDelegateHandle OnDestroySessionCompleteDelegateHandle;

	virtual void Init() override;

	void OnCreateSessionComplete(FName SessionName, bool bWasSuccessful);

	void OnStartOnlineSessionComplete(FName SessionName, bool bWasSuccessful);

	void OnFindSessionsComplete(bool bWasSuccessful);

	virtual void OnDestroySessionComplete(FName SessionName, bool bWasSuccessful);

	TSharedPtr<FOnlineSessionSearch> SessionsSearch;

public:

	USGameInstance(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(BlueprintAssignable, Category = "Sessions")
	FFindSessionsCompleteEvent SessionFindCompleteEvent;

	UFUNCTION(BlueprintCallable, Category = "Sessions")
	bool HostSession(FSUniqueNetId UserID,FSServerSettings HostSettings, FName SessionName);

	UFUNCTION(BlueprintCallable, Category = "Sessions")
	void FindSessions(FSUniqueNetId UserID, FSSearchSettings SearchSettings);

	UFUNCTION(BlueprintCallable, Category = "Sessions")
	void DestroySessionAndLeaveGame();

	void ServerTravelToLobby();
};