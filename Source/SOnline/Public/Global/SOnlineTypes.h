// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/PlayerState.h"
#include "Engine/Texture2D.h"

#include "OnlineSubsystem.h"
#include "Online.h"
#include "OnlineSubsystemImpl.h"
#include "OnlineSubsystemUtils.h"
#include "OnlineSubsystemUtilsModule.h"

#include "SOnlineTypes.generated.h"


UENUM()
enum class EOnlineMode : uint8
{
	Offline,
	LAN,
	Online
};


UENUM()
enum class EBlueprintAsyncResultSwitch : uint8
{
	// On Success
	OnSuccess,
	// Still loading
	AsyncLoading,
	// On Failure
	OnFailure
};


/////////////// ONLINE SUBSYSTEM STRUCTS ///////////////

UENUM(BlueprintType)
enum class ESOnlinePresenceState : uint8
{
	OPS_Online			UMETA(DisplayName = "Online"),
	OPS_Offline			UMETA(DisplayName = "Offline"),
	OPS_Away			UMETA(DisplayName = "Away"),
	OPS_ExtendedAway	UMETA(DisplayName = "ExtendedAway"),
	OPS_DoNotDisturb	UMETA(DisplayName = "DoNotDistrub"),
	OPS_Chat			UMETA(DisplayName = "Chat")
};

UENUM(BlueprintType)
enum class EOnlineComparisonOpRedux : uint8
{
	Equals,
	NotEquals,
	GreaterThan,
	GreaterThanEquals,
	LessThan,
	LessThanEquals,
};

UENUM(BlueprintType)
enum class EServerPresenceSearchType : uint8
{
	AllServers,
	ClientServersOnly,
	DedicatedServersOnly
};

UENUM()
enum class ESessionSettingSearchResult : uint8
{
	// Found the setting
	Found,

	// Did not find the setting
	NotFound,

	// Was not the correct type
	WrongType
};

UENUM()
enum class EBlueprintResultSwitch : uint8
{
	// On Success
	OnSuccess,

	// On Failure
	OnFailure
};

USTRUCT(BlueprintType)
struct FSSessionPropertyKeyPair
{
	GENERATED_USTRUCT_BODY()

		FName Key;
	FVariantData Data;
};

USTRUCT(BlueprintType)
struct FSessionsSearchSetting
{
	GENERATED_USTRUCT_BODY()
		//UPROPERTY()


		// Had to make a copy of this to account for the original not being exposed to blueprints
		/** How is this session setting compared on the backend searches */
	EOnlineComparisonOpRedux ComparisonOp;

	// The key pair to search for
	FSSessionPropertyKeyPair PropertyKeyPair;

};

USTRUCT(BlueprintType)
struct FSSearchSettings
{
	GENERATED_USTRUCT_BODY()

	FSSearchSettings()
	{
		ServerSearchType = EServerPresenceSearchType::AllServers;
		MaxResults = 100;
		bNonEmptyServersOnly = false;
		bNonEmptyServersOnly = false;
		bSecureServersOnly = false;
		bIsLAN = false;
		PingBucketSize = 50;
		MinSlotsAvailable = 0;
		UseFilters = false;
		bIsPresence = true;
	}

	// Whether or not to search for dedicated servers
	UPROPERTY(BlueprintReadWrite)
	EServerPresenceSearchType ServerSearchType;


	UPROPERTY(BlueprintReadWrite)
	bool UseFilters; // ������� ��-�� ������������� ������������� ������� ������� � ����������

	// Store extra settings
	UPROPERTY(BlueprintReadWrite)
	TArray<FSessionsSearchSetting> Filters;

	// Maximum number of results to return
	UPROPERTY(BlueprintReadWrite)
	int MaxResults;

	// Search for empty servers only
	UPROPERTY(BlueprintReadWrite)
	bool bEmptyServersOnly;

	// Search for non empty servers only
	UPROPERTY(BlueprintReadWrite)
	bool bNonEmptyServersOnly;

	// Search for secure servers only
	UPROPERTY(BlueprintReadWrite)
	bool bSecureServersOnly;

	UPROPERTY(BlueprintReadWrite)
	bool bIsLAN;

	UPROPERTY(BlueprintReadWrite)
	bool bIsPresence;

	UPROPERTY(BlueprintReadWrite)
	int PingBucketSize;

	// Min slots requires to search
	UPROPERTY(BlueprintReadWrite)
	int MinSlotsAvailable;
};

USTRUCT(BlueprintType)
struct FSServerSettings
{
	GENERATED_USTRUCT_BODY()

	FSServerSettings()
	{
		NumPublicConnections = 4;
		NumPrivateConnections = 2;
		bUseLAN = false;
		bAllowInvites = true;
		bDedicatedServer = false;
		bUsePresence = true;
		bAllowJoinViaPresence = true;
		bAllowJoinViaPresenceFriendsOnly = false;
		bAntiCheatProtected = false;
		bUsesStats = false;
		bShouldAdvertise = true;
		bAllowJoinInProgress = true;
	}

	UPROPERTY(BlueprintReadWrite)
	TArray<FSSessionPropertyKeyPair> ExtraSettings;

	// Number of public connections
	UPROPERTY(BlueprintReadWrite)
	int NumPublicConnections;

	// Number of private connections
	UPROPERTY(BlueprintReadWrite)
	int NumPrivateConnections;

	// Whether or not to search LAN
	UPROPERTY(BlueprintReadWrite)
	bool bUseLAN;

	// Whether or not to allow invites
	UPROPERTY(BlueprintReadWrite)
	bool bAllowInvites;

	// Whether this is a dedicated server or not
	UPROPERTY(BlueprintReadWrite)
	bool bDedicatedServer;

	// Whether to use the presence option
	UPROPERTY(BlueprintReadWrite)
	bool bUsePresence;

	// Whether to allow joining via presence
	UPROPERTY(BlueprintReadWrite)
	bool bAllowJoinViaPresence;

	// Allow joining via presence for friends only
	UPROPERTY(BlueprintReadWrite)
	bool bAllowJoinViaPresenceFriendsOnly;

	// Delcare the server to be anti cheat protected
	UPROPERTY(BlueprintReadWrite)
	bool bAntiCheatProtected;

	// Record Stats
	UPROPERTY(BlueprintReadWrite)
	bool bUsesStats;

	// Should advertise server?
	UPROPERTY(BlueprintReadWrite)
	bool bShouldAdvertise;

	UPROPERTY(BlueprintReadWrite)
	bool bAllowJoinInProgress;

	FOnlineSessionSettings GetSetingsForSession()
	{
		FOnlineSessionSettings Settings;
		Settings.NumPublicConnections = NumPublicConnections;
		Settings.NumPrivateConnections = NumPrivateConnections;
		Settings.bShouldAdvertise = bShouldAdvertise;
		Settings.bAllowJoinInProgress = bAllowJoinInProgress;
		Settings.bIsLANMatch = bUseLAN;
		Settings.bAllowJoinViaPresence = bAllowJoinViaPresence;
		Settings.bIsDedicated = bDedicatedServer;
		Settings.bUsesPresence = bUsePresence;
		Settings.bAllowJoinViaPresenceFriendsOnly = bAllowJoinViaPresenceFriendsOnly;
		Settings.bAntiCheatProtected = bAntiCheatProtected;
		Settings.bUsesStats = bUsesStats;
		Settings.bAllowInvites = bAllowInvites;

		FOnlineSessionSetting ExtraSetting;
		for (int i = 0; i < ExtraSettings.Num(); i++)
		{
			ExtraSetting.Data = ExtraSettings[i].Data;
			ExtraSetting.AdvertisementType = EOnlineDataAdvertisementType::ViaOnlineService;
			Settings.Settings.Add(ExtraSettings[i].Key, ExtraSetting);
		}

		return Settings;
	}
};

class FOnlineSearchSettingsEx : public FOnlineSearchSettings
{
	/**
	*	Sets a key value pair combination that defines a search parameter
	*
	* @param Key key for the setting
	* @param Value value of the setting
	* @param InType type of comparison
	*/
public:

	void HardSet(FName Key, const FVariantData& Value, EOnlineComparisonOpRedux CompOp)
	{
		FOnlineSessionSearchParam* SearchParam = SearchParams.Find(Key);

		TEnumAsByte<EOnlineComparisonOp::Type> op;

		switch (CompOp)
		{
		case EOnlineComparisonOpRedux::Equals: op = EOnlineComparisonOp::Equals; break;
		case EOnlineComparisonOpRedux::GreaterThan: op = EOnlineComparisonOp::GreaterThan; break;
		case EOnlineComparisonOpRedux::GreaterThanEquals: op = EOnlineComparisonOp::GreaterThanEquals; break;
		case EOnlineComparisonOpRedux::LessThan: op = EOnlineComparisonOp::LessThan; break;
		case EOnlineComparisonOpRedux::LessThanEquals: op = EOnlineComparisonOp::LessThanEquals; break;
		case EOnlineComparisonOpRedux::NotEquals: op = EOnlineComparisonOp::NotEquals; break;
		default: op = EOnlineComparisonOp::Equals; break;
		}

		if (SearchParam)
		{
			SearchParam->Data = Value;
			SearchParam->ComparisonOp = op;
		}
		else
		{
			FOnlineSessionSearchParam searchSetting((int)0, op);
			searchSetting.Data = Value;
			SearchParams.Add(Key, searchSetting);
		}
	}
};

struct FOnlineSubsystemCallHelperAdvanced
{
public:
	FOnlineSubsystemCallHelperAdvanced(const TCHAR* CallFunctionContext, UWorld* World, FName SystemName = NAME_None)
		: OnlineSub(Online::GetSubsystem(World, SystemName))
		, FunctionContext(CallFunctionContext)
	{
		if (OnlineSub == nullptr)
		{
			FFrame::KismetExecutionMessage(*FString::Printf(TEXT("%s - Invalid or uninitialized OnlineSubsystem"), FunctionContext), ELogVerbosity::Warning);
		}
	}

	void QueryIDFromPlayerController(APlayerController* PlayerController)
	{
		UserID.Reset();
		//return const_cast<FUniqueNetId*>(UniqueNetIdPtr);
		if (APlayerState* PlayerState = (PlayerController != NULL) ? PlayerController->PlayerState : NULL)
		{
			UserID = PlayerState->UniqueId.GetUniqueNetId();
			if (!UserID.IsValid())
			{
				FFrame::KismetExecutionMessage(*FString::Printf(TEXT("%s - Cannot map local player to unique net ID"), FunctionContext), ELogVerbosity::Warning);
			}
		}
		else
		{
			FFrame::KismetExecutionMessage(*FString::Printf(TEXT("%s - Invalid player state"), FunctionContext), ELogVerbosity::Warning);
		}
	}


	bool IsValid() const
	{
		return UserID.IsValid() && (OnlineSub != nullptr);
	}

public:
	//TSharedPtr<const FUniqueNetId>& GetUniqueNetId()
	TSharedPtr</*class*/ const FUniqueNetId> UserID;
	IOnlineSubsystem* const OnlineSub;
	const TCHAR* FunctionContext;
};



USTRUCT(BlueprintType)
struct FSUniqueNetId
{
	GENERATED_USTRUCT_BODY()

private:
	bool bUseDirectPointer;


public:
	TSharedPtr<const FUniqueNetId> UniqueNetId;
	const FUniqueNetId * UniqueNetIdPtr;

	void SetUniqueNetId(const TSharedPtr<const FUniqueNetId> &ID)
	{
		bUseDirectPointer = false;
		UniqueNetIdPtr = nullptr;
		UniqueNetId = ID;
	}

	void SetUniqueNetId(const FUniqueNetId *ID)
	{
		bUseDirectPointer = true;
		UniqueNetIdPtr = ID;
	}

	bool IsValid() const
	{
		if (bUseDirectPointer && UniqueNetIdPtr != nullptr && UniqueNetIdPtr->IsValid())
		{
			return true;
		}
		else if (UniqueNetId.IsValid())
		{
			return true;
		}
		else
			return false;

	}

	const FUniqueNetId* GetUniqueNetId() const
	{
		if (bUseDirectPointer && UniqueNetIdPtr != nullptr)
		{
			// No longer converting to non const as all functions now pass const UniqueNetIds
			return /*const_cast<FUniqueNetId*>*/(UniqueNetIdPtr);
		}
		else if (UniqueNetId.IsValid())
		{
			return UniqueNetId.Get();
		}
		else
			return nullptr;
	}

	FSUniqueNetId()
	{
		bUseDirectPointer = false;
		UniqueNetIdPtr = nullptr;
	}
};

USTRUCT(BlueprintType)
struct FSFriendPresenceInfo
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(BlueprintReadOnly, Category = "Online|Friend")
		bool bIsOnline;
	UPROPERTY(BlueprintReadOnly, Category = "Online|Friend")
		bool bIsPlaying;
	UPROPERTY(BlueprintReadOnly, Category = "Online|Friend")
		bool bIsPlayingThisGame;
	UPROPERTY(BlueprintReadOnly, Category = "Online|Friend")
		bool bIsJoinable;
	UPROPERTY(BlueprintReadOnly, Category = "Online|Friend")
		bool bHasVoiceSupport;
	UPROPERTY(BlueprintReadOnly, Category = "Online|Friend")
		ESOnlinePresenceState PresenceState;
	UPROPERTY(BlueprintReadOnly, Category = "Online|Friend")
		FString StatusString;
};

USTRUCT(BlueprintType)
struct FSFriendInfo
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(BlueprintReadOnly, Category = "Online|Friend")
		FString DisplayName;
	UPROPERTY(BlueprintReadOnly, Category = "Online|Friend")
		FString RealName;
	UPROPERTY(BlueprintReadOnly, Category = "Online|Friend")
		FSUniqueNetId UniqueNetId;
	UPROPERTY(BlueprintReadOnly, Category = "Online|Friend")
		FSFriendPresenceInfo PresenceInfo;
};

/////////////// STEAM TYPES /////////////////

UENUM(Blueprintable)
enum class ESteamAvatarSize : uint8
{
	SteamAvatar_Small = 1,
	SteamAvatar_Medium = 2,
	SteamAvatar_Large = 3
};
