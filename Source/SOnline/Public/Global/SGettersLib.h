// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "SGameplayTypes.h"
#include "SGettersLib.generated.h"

class ASOnlineGameState;
class ASPlayerController;
class ASPlayerState;
class ASOnlineCharacter;
class ASOnlineGameMode;
class ASGameModeBase;
class ASGameStateBase;
class USGameInstance;

UCLASS()
class SONLINE_API USGettersLib : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	static USGameInstance* SGetGameInstance(AActor* ActorForGetWorld);

	static ASOnlineGameMode* SGetSonlineGameMode(AActor* ActorForGetWorld);

	static ASGameModeBase* SGetGameModeBase(AActor* ActorForGetWorld);
	
	static ASOnlineGameState* SGetSOnlineGameState(AActor* ActorForGetWorld);

	static ASGameStateBase* SGetGameStateBase(AActor* ActorForGetWorld);
	
	static ASPlayerState* SGetPlayerState(APlayerController* Contoller);

	static ASPlayerState* SGetPlayerState(AController* Contoller);

	static ASPlayerState* SGetPlayerState(ASOnlineCharacter* Character);

	static ASPlayerState* SGetPlayerState(APlayerState* PlayerState);
	
	static ASPlayerController* SGetController(AController* Controller);
	
	static ASPlayerController* SGetController(ASOnlineCharacter* Character);

	static APlayerController* GetPlayerController(AController* Controller);
	
	static ASOnlineCharacter* SGetCharacter(AController* Controller);
	
	static ASOnlineCharacter* SGetCharacter(ASPlayerController* Controller);
	
	static ASOnlineCharacter* SGetCharacter(APlayerState* PlayerState);

	static EGameState SGetCurrentGameState(AActor* ActorForGetWorld);
	
};
