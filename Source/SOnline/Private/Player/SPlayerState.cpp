// Fill out your copyright notice in the Description page of Project Settings.

#include "SPlayerState.h"
#include "SOnline.h"
#include "TimerManager.h"
#include "SGettersLib.h"
#include "SOnlineGameState.h"
#include "Net/UnrealNetwork.h"
#include "SOnlineCharacter.h"


ASPlayerState::ASPlayerState()
{
	Alive = false;
	Kills = 0;
	Deaths = 0;
	ReadyForGame = false;
}

void ASPlayerState::BeginPlay()
{
	Super::BeginPlay();
}


void ASPlayerState::SetReadyForGame_Implementation(bool ReadyStatus)
{
	if(ReadyForGame == ReadyStatus)
	{
		return;
	}
	else
	{
		ReadyForGame = ReadyStatus;

		USGettersLib::SGetSOnlineGameState(this)->PlayerReady(this);
	}
}

bool ASPlayerState::SetReadyForGame_Validate(bool ReadyStatus)
{
	return true;
}

void ASPlayerState::SetAlivePlayer_Implementation(bool NewAlive)
{
	Alive = NewAlive;
}

void ASPlayerState::AddKill(int Killed)
{
	Kills += Killed;
}

void ASPlayerState::AddDeath()
{
	Deaths++;
}

void ASPlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASPlayerState, Kills);
	DOREPLIFETIME(ASPlayerState, Deaths);
	DOREPLIFETIME(ASPlayerState, Alive);
	DOREPLIFETIME(ASPlayerState, ReadyForGame);
}