// Fill out your copyright notice in the Description page of Project Settings.

#include "SWeaponManager.h"
#include "Net/UnrealNetwork.h"
#include "SWeaponBase.h"
#include "SOnlineCharacter.h"

USWeaponManager::USWeaponManager()
{
	CurrentSlot = ESlotType::None;
	AttachedSlot = ESlotType::None;

	bReplicates = true;
}

void USWeaponManager::BeginPlay()
{
	Super::BeginPlay();
	
	OwnerCharacter = Cast<ASOnlineCharacter>(GetOwner());
}

bool USWeaponManager::AbleToAddWeapon(ASWeaponBase* Weapon)
{
	if (!Weapon) { return false; }
	ESlotType AddToSlot = Weapon->GetSlotType();
	switch (AddToSlot)
	{
	case ESlotType::Basic: if(Slot_Basic){return false; } else {return true; } break;
	case ESlotType::Secondary: if(Slot_Secondary){return false; } else {return true; } break;
	case ESlotType::Additional:if(Slot_Additional){return false; } else {return true; } break;
	case ESlotType::Belt:if(AbleToAddBelt(Weapon)){return false; } else {return true; } break;
	default: return false; break;
	}
	return false;
}

bool USWeaponManager::AbleToAddBelt(ASWeaponBase* Weapon)
{
	return true;
}

void USWeaponManager::AddWeapon_Implementation(ASWeaponBase* Weapon)
{
	if (AbleToAddWeapon(Weapon))
	{
		ESlotType AddToSlot = Weapon->GetSlotType();
		switch (AddToSlot)
		{
		case ESlotType::Basic: Slot_Basic = Weapon; break;
		case ESlotType::Secondary: Slot_Secondary = Weapon; break;
		case ESlotType::Additional: Slot_Additional = Weapon; break;
		case ESlotType::Belt: Slot_Belt.Add(Weapon); break;
		default: break;
		}
	}
	else { return; }

	Weapon->GetMesh()->SetSimulatePhysics(false);
	Weapon->GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Weapon->Instigator = OwnerCharacter;
	Weapon->WeaponCollision.AddIgnoredActor(OwnerCharacter);
	Weapon->SetActorEnableCollision(false);
	Weapon->SetOwner(Weapon->GetInstigatorController());
	Weapon->SetWeaponOwner(OwnerCharacter);

	WeaponAttach(Weapon);
}

void USWeaponManager::WeaponAttach(ASWeaponBase* Weapon)
{
	if (!Weapon) { return; }

	ESlotType AttachToSlot = Weapon->GetSlotType();

	FAttachmentTransformRules ParamsAttach(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepRelative, true);
	FName SocketName(TEXT("None"));

	switch (AttachToSlot)
	{
	case ESlotType::Basic:
		SocketName = TEXT("SK_Basic");
		break;
	case ESlotType::Secondary:
		SocketName = TEXT("SK_Secondary");
		break;
	case ESlotType::Additional:
		SocketName = TEXT("None");
		break;
	case ESlotType::Belt:
		SocketName = TEXT("None");
		break;
	case ESlotType::None:
		return;
		break;
	default:
		break;
	}

	Weapon->AttachToComponent(OwnerCharacter->GetMesh(), ParamsAttach, SocketName);
}

bool USWeaponManager::SelectWeapon(ESlotType SelectSlot)
{
	if (CurrentSlot == SelectSlot) { return false; }

	if (SelectSlot == ESlotType::None)
	{
		SelectWeaponToServer(SelectSlot);
		return true;
	}

	if (GetWeaponFromSlot(SelectSlot))
	{
		SelectWeaponToServer(SelectSlot);
		return true;
	}

	return false;
}

void USWeaponManager::SelectWeaponToServer_Implementation(ESlotType SelectSlot)
{
	if (SelectSlot == ESlotType::None)
	{
		CurrentSlot = SelectSlot;

		ChangedWeapon(SelectSlot);
	}

	if (GetWeaponFromSlot(SelectSlot))
	{
		CurrentSlot = SelectSlot;

		ChangedWeapon(SelectSlot);
	}
}

bool USWeaponManager::SelectWeaponToServer_Validate(ESlotType SelectSlot)
{
	return true;
}

void USWeaponManager::ChangedWeapon_Implementation(ESlotType NewSlot)
{
	if(GetWeaponFromSlot(AttachedSlot)){ GetWeaponFromSlot(AttachedSlot)->ChangeWeaponAction(); }
	WeaponAttach(GetWeaponFromSlot(AttachedSlot));

	AttachedSlot = NewSlot;

	if (NewSlot != ESlotType::None)
	{
		FAttachmentTransformRules ParamsAttach(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepRelative, true);
		FName SocketName(TEXT("SK_Weapon"));

		GetWeaponFromSlot(NewSlot)->AttachToComponent(OwnerCharacter->GetMesh(), ParamsAttach, SocketName);
	}

	OnChangeWeapon.Broadcast(NewSlot);
}

ASWeaponBase* USWeaponManager::GetCurrentWeapon()
{
	return GetWeaponFromSlot(CurrentSlot);
}

ASWeaponBase* USWeaponManager::GetWeaponFromSlot(ESlotType SlotToGet)
{
	switch (SlotToGet)
	{
	case ESlotType::Basic:
		return Slot_Basic;
		break;
	case ESlotType::Secondary:
		return Slot_Secondary;
		break;
	case ESlotType::Additional:
		return Slot_Additional;
		break;
	case ESlotType::Belt:
		break;
	case ESlotType::None:
		return nullptr;
		break;
	default:
		break;
	}

	return nullptr;
}

void USWeaponManager::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(USWeaponManager, CurrentSlot);
	DOREPLIFETIME(USWeaponManager, Slot_Basic);
	DOREPLIFETIME(USWeaponManager, Slot_Secondary);
	DOREPLIFETIME(USWeaponManager, Slot_Additional);
	DOREPLIFETIME(USWeaponManager, Slot_Belt);
}
