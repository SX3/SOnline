// Fill out your copyright notice in the Description page of Project Settings.

#include "SPlayerController.h"
#include "TimerManager.h"
#include "SGettersLib.h"
#include "SGameModeBase.h"

ASPlayerController::ASPlayerController()
{

}

void ASPlayerController::LaunchRespawnTimer(float Time)
{
	this->GetWorld()->GetTimerManager().SetTimer(RespawnTimer, this, &ASPlayerController::Respawn, Time, false);
}

void ASPlayerController::Respawn()
{
	this->GetWorld()->GetTimerManager().ClearTimer(RespawnTimer);
	USGettersLib::SGetGameModeBase(this)->SpawnPlayer(this);
}
