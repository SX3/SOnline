// Fill out your copyright notice in the Description page of Project Settings.

#include "SOnlineGameSession.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionInterface.h"
#include "OnlineSessionSettings.h"
#include "Kismet/GameplayStatics.h"
#include "OnlineSubsystemSessionSettings.h"

ASOnlineGameSession::ASOnlineGameSession()
{

}

void ASOnlineGameSession::BeginPlay()
{
	Super::BeginPlay();
}



void ASOnlineGameSession::RegisterServer()
{
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();

	if (OnlineSub)
	{
		IOnlineSessionPtr SessionInterface = OnlineSub->GetSessionInterface();
		if (SessionInterface)
		{
			TSharedPtr<class FOnlineSessionSettings> Settings;

			Settings = MakeShareable(new FOnlineSessionSettings);

			Settings->Set(SETTING_GAMEMODE, FString("SOnlineMode"), EOnlineDataAdvertisementType::ViaOnlineService);
			Settings->Set(SETTING_MAPNAME, GetWorld()->GetMapName(), EOnlineDataAdvertisementType::ViaOnlineService);
			Settings->Set(SETTING_MATCHING_HOPPER, FString("SDeathmatch"), EOnlineDataAdvertisementType::DontAdvertise);
			Settings->Set(SETTING_MATCHING_TIMEOUT, 120.0f, EOnlineDataAdvertisementType::ViaOnlineService);
			Settings->Set(SETTING_SESSION_TEMPLATE_NAME, FString("SOnlineGameSession"), EOnlineDataAdvertisementType::DontAdvertise);

			Settings->bIsDedicated = true;
			Settings->bIsLANMatch = false;
			Settings->bAllowInvites = true;
			Settings->bAllowJoinInProgress = true;
			Settings->bShouldAdvertise = true;
			Settings->NumPublicConnections = 99;
			Settings->NumPrivateConnections = 99;
			Settings->bUsesPresence = true;


			SessionInterface->CreateSession(0, GameSessionName, *Settings);
			UE_LOG(LogTemp, Log, TEXT("=======REGISTERED SERVER======="))
		}
	}
}