// Fill out your copyright notice in the Description page of Project Settings.

#include "SGetFriendsCallbackProxy.h"

USGetFriendsCallbackProxy::USGetFriendsCallbackProxy(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, FriendListReadCompleteDelegate(FOnReadFriendsListComplete::CreateUObject(this, &ThisClass::OnReadFriendsListCompleted))
{
}

USGetFriendsCallbackProxy* USGetFriendsCallbackProxy::SGetFriendsList(UObject* WorldContextObject, class APlayerController* PlayerController)
{
	USGetFriendsCallbackProxy* Proxy = NewObject<USGetFriendsCallbackProxy>();
	Proxy->PlayerControllerWeakPtr = PlayerController;
	Proxy->WorldContextObject = WorldContextObject;
	return Proxy;
}

void USGetFriendsCallbackProxy::Activate()
{
	if (!PlayerControllerWeakPtr.IsValid())
	{
		// Fail immediately
		TArray<FSFriendInfo> EmptyArray;
		OnFailure.Broadcast(EmptyArray);
		return;
	}

	IOnlineFriendsPtr Friends = Online::GetFriendsInterface();
	if (Friends.IsValid())
	{
		ULocalPlayer* Player = Cast<ULocalPlayer>(PlayerControllerWeakPtr->Player);

		Friends->ReadFriendsList(Player->GetControllerId(), EFriendsLists::ToString((EFriendsLists::Default)), FriendListReadCompleteDelegate);
		return;
	}

	// Fail immediately
	TArray<FSFriendInfo> EmptyArray;

	OnFailure.Broadcast(EmptyArray);
}

void USGetFriendsCallbackProxy::OnReadFriendsListCompleted(int32 LocalUserNum, bool bWasSuccessful, const FString& ListName, const FString& ErrorString)
{
	if (bWasSuccessful)
	{
		IOnlineFriendsPtr Friends = Online::GetFriendsInterface();
		if (Friends.IsValid())
		{
			// Not actually needed anymore, plus was not being validated and causing a crash
			//ULocalPlayer* Player = Cast<ULocalPlayer>(PlayerControllerWeakPtr->Player);

			TArray<FSFriendInfo> FriendsListOut;
			TArray<TSharedRef<FOnlineFriend>> FriendList;
			Friends->GetFriendsList(LocalUserNum, ListName, FriendList);

			for (int32 i = 0; i < FriendList.Num(); i++)
			{
				TSharedRef<FOnlineFriend> Friend = FriendList[i];
				FSFriendInfo FRInf;
				FOnlineUserPresence Upres = Friend->GetPresence();

				FRInf.DisplayName = Friend->GetDisplayName();
				FRInf.RealName = Friend->GetRealName();
				FRInf.UniqueNetId.SetUniqueNetId(Friend->GetUserId());

				FRInf.PresenceInfo.bIsJoinable = Upres.bIsOnline;
				FRInf.PresenceInfo.bHasVoiceSupport = Upres.bHasVoiceSupport;
				FRInf.PresenceInfo.bIsPlaying = Upres.bIsPlaying;
				FRInf.PresenceInfo.PresenceState = (ESOnlinePresenceState)((int32)Upres.Status.State);
				FRInf.PresenceInfo.bIsJoinable = Upres.bIsJoinable;
				FRInf.PresenceInfo.bIsPlayingThisGame = Upres.bIsPlayingThisGame;

				FriendsListOut.Add(FRInf);
			}

			OnSuccess.Broadcast(FriendsListOut);
		}
	}
	else
	{
		TArray<FSFriendInfo> EmptyArray;
		OnFailure.Broadcast(EmptyArray);
	}
}