// Fill out your copyright notice in the Description page of Project Settings.

#include "SWeaponBase.h"
#include "Net/UnrealNetwork.h"
#include "SOnlineCharacter.h"
#include "SWeaponManager.h"
#include "SOnlineCharacter.h"
#include "TimerManager.h"

ASWeaponBase::ASWeaponBase()
{
	PrimaryActorTick.bCanEverTick = false;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	SetRootComponent(WeaponMesh);

	bReplicates = true;

	WeaponCollision.AddIgnoredActor(this);
	WeaponCollision.bTraceComplex = true;

	UseRate = 300.f;
	AutoUse = false;
	WeaponDamage = 20.f;
	SlotType = ESlotType::Basic;
	WeaponType = EWeaponType::Pistol;

	WeaponName = (TEXT("NotName"));

	NetUpdateFrequency = 66.0f;
	MinNetUpdateFrequency = 33.0f;
}

void ASWeaponBase::BeginPlay()
{
	Super::BeginPlay();

	TimeBetweenUsed = 60 / UseRate;
}

void ASWeaponBase::Interact(ASOnlineCharacter* InteractPlayer)
{
	if (!InteractPlayer) { return; }

	InteractPlayer->WeaponManager->AddWeapon(this);
}

			///// USE WEAPON /////

void ASWeaponBase::UseWeapon(bool Start_Stop)
{
	if (Start_Stop)
	{
		ServerStartUseWeapon();
	}
	else
	{
		ServerStopUseWeapon();
	}
}

void ASWeaponBase::ServerStartUseWeapon_Implementation()
{
	ClientStartUseWeapon();
}

bool ASWeaponBase::ServerStartUseWeapon_Validate()
{
	return true;
}

void ASWeaponBase::ServerStopUseWeapon_Implementation()
{
	ClientStopUseWeapon();
}

bool ASWeaponBase::ServerStopUseWeapon_Validate()
{
	return true;
}

void ASWeaponBase::ClientStartUseWeapon_Implementation()
{
	float FirstDelay = FMath::Max(LastUsedTime + TimeBetweenUsed - GetWorld()->TimeSeconds, 0.0f);

	GetWorldTimerManager().SetTimer(UseCooldownTimer, this, &ASWeaponBase::UseWeaponLogic, TimeBetweenUsed, AutoUse, FirstDelay);
}

void ASWeaponBase::ClientStopUseWeapon_Implementation()
{
	GetWorldTimerManager().ClearTimer(UseCooldownTimer);
}

void ASWeaponBase::UseWeaponLogic()
{
	LastUsedTime = GetWorld()->TimeSeconds;
}

			///// GETTERS /////

ESlotType ASWeaponBase::GetSlotType()
{
	return SlotType;
}

void ASWeaponBase::ChangeWeaponAction()
{
	GetWorldTimerManager().ClearTimer(UseCooldownTimer);
}

USkeletalMeshComponent* ASWeaponBase::GetMesh()
{
	return WeaponMesh;
}

///// SETTERS /////

void ASWeaponBase::SetWeaponOwner(ASOnlineCharacter* NewOwner)
{
	WeaponOwner = NewOwner;
}

