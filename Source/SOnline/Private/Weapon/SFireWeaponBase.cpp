// Fill out your copyright notice in the Description page of Project Settings.

#include "SFireWeaponBase.h"
#include "Kismet/GameplayStatics.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "SOnlineCharacter.h"
#include "Net/UnrealNetwork.h"
#include "TimerManager.h"


#ifdef SWEAPON_DEBUG
#include "DrawDebugHelpers.h"
#endif

ASFireWeaponBase::ASFireWeaponBase()
{
	FireFlashSocketName = "SK_GunFire";

	FireStartDistance = 300.0f;
	FireDistance = 10000.0f;

	MaxAmmoInClip = 30;
	TotalAmmo = 90;
	CurrentAmmoInClip = 30;

	ReloadTime = 5.0f;
	RecoilForce = 0.3f;
}

void ASFireWeaponBase::Reload()
{
	if (!Reloading)
	{
		if (CurrentAmmoInClip != MaxAmmoInClip)
		{
			if (TotalAmmo > 0)
			{
				Reloading = true;

				GetWorldTimerManager().SetTimer(ReloadTimer, this, &ASFireWeaponBase::ReloadComplete, ReloadTime, false);
			}
		}	
	}
}

void ASFireWeaponBase::ReloadComplete()
{
	GetWorldTimerManager().ClearTimer(ReloadTimer);

	int AmmoToReload = MaxAmmoInClip - CurrentAmmoInClip;

	if (TotalAmmo >= AmmoToReload)
	{
		TotalAmmo -= AmmoToReload;
		CurrentAmmoInClip += AmmoToReload;
	}
	else
	{
		CurrentAmmoInClip += TotalAmmo;
		TotalAmmo = 0;
	}

	Reloading = false;
}

void ASFireWeaponBase::ChangeWeaponAction()
{
	Super::ChangeWeaponAction();

	GetWorldTimerManager().ClearTimer(ReloadTimer);
}

void ASFireWeaponBase::UseWeaponLogic()
{
	if (CurrentAmmoInClip > 0)
	{
		Super::UseWeaponLogic();

		if (Role == ROLE_Authority)
		{
			ServerShotTrace();

			if (GetNetMode() == NM_ListenServer)
			{
				ClientShotTrace();
			}
		}
		else
		{
			ClientShotTrace();
		}

		CurrentAmmoInClip--;
	}
	else
	{
		if (TotalAmmo > 0)
		{
			Reload();
		}
	}
}

void ASFireWeaponBase::ServerShotTrace()
{
	FVector TraceStartLocation;
	FRotator ViewRotation;
	FVector TraceEndLocation;

	WeaponOwner->GetActorEyesViewPoint(TraceStartLocation, ViewRotation);

	TraceStartLocation = TraceStartLocation + WeaponOwner->GetCameraForwardVector() * FireStartDistance;

	TraceEndLocation = TraceStartLocation + (ViewRotation.Vector() * FireDistance);

	FVector ShotDirection = ViewRotation.Vector();

	WeaponCollision.bReturnPhysicalMaterial = true;

	FHitResult HitFire;
	if (GetWorld()->LineTraceSingleByChannel(HitFire, TraceStartLocation, TraceEndLocation, ECC_GameTraceChannel1, WeaponCollision))
	{
		AActor* HitActor = HitFire.GetActor();

		UGameplayStatics::ApplyPointDamage(HitActor, WeaponDamage, ShotDirection, HitFire, WeaponOwner->GetInstigatorController(), this, DamageType);
	}

#ifdef SWEAPON_DEBUG
	DrawDebugLine(GetWorld(), TraceStartLocation, TraceEndLocation, FColor::Green, false, 5.0f, 0, 1.0f);
#endif
}

void ASFireWeaponBase::ClientShotTrace()
{
	FVector TraceStartLocation;
	FRotator ViewRotation;
	FVector TraceEndLocation;
	WeaponOwner->GetActorEyesViewPoint(TraceStartLocation, ViewRotation);

	TraceStartLocation = TraceStartLocation + WeaponOwner->GetCameraForwardVector() * FireStartDistance;

	TraceEndLocation = TraceStartLocation + (WeaponOwner->LookAngles.Vector() * FireDistance);

	FVector ShotDirection = ViewRotation.Vector();

	WeaponCollision.bReturnPhysicalMaterial = true;

	FHitResult HitFire;
	if (GetWorld()->LineTraceSingleByChannel(HitFire, TraceStartLocation, TraceEndLocation, ECC_GameTraceChannel1, WeaponCollision))
	{
		PlayEffects(HitFire.ImpactPoint, HitFire.ImpactNormal.Rotation(),HitFire.PhysMaterial.Get()->SurfaceType, true);
	}
	else
	{
		PlayEffects(TraceEndLocation, HitFire.ImpactNormal.Rotation(), HitFire.PhysMaterial.Get()->SurfaceType, false);
	}

}

void ASFireWeaponBase::PlayEffects(FVector TraceTo, FRotator RotationNormal, EPhysicalSurface SurfaceType, bool IsImpact)
{
	FVector TrailLocation = WeaponMesh->GetSocketLocation("SK_GunFire");

	if (IsImpact)
	{
		switch (SurfaceType)
		{
		case SurfaceType_Default:	break;
		case CONCRETE_SURFACE: if(ImpactParticles.ImpactConcrete){ UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactParticles.ImpactConcrete, TraceTo, RotationNormal); }	break;
		case METAL_SURFACE: if(ImpactParticles.ImpactMetal) { UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactParticles.ImpactMetal, TraceTo, RotationNormal); } break;
		case GLASS_SURFACE: if(ImpactParticles.ImpactGlass) { UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactParticles.ImpactGlass, TraceTo, RotationNormal); } break;
		case WOOD_SURFACE: if(ImpactParticles.ImpactWood) { UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactParticles.ImpactWood, TraceTo, RotationNormal); } break;
		case MEAT_SURFACE: if(ImpactParticles.ImpactMeat) { UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactParticles.ImpactMeat, TraceTo, RotationNormal); } break;
		default: break;
		}
	}

	if (FireFlashParticle){ UGameplayStatics::SpawnEmitterAttached(FireFlashParticle, WeaponMesh, FireFlashSocketName); }

	if (TrailParticle)
	{
		UParticleSystemComponent* TrailComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TrailParticle, TrailLocation);

		if (TrailComp)
		{
			TrailComp->SetVectorParameter("Target", TraceTo);
		}
	}

	if (UseCameraShake)
	{
		if (WeaponOwner)
		{
			APlayerController* PC = Cast<APlayerController>(WeaponOwner->GetController());
			if (PC)
			{
				WeaponOwner->AddControllerPitchInput(FMath::FRandRange(0, RecoilForce * -1));
				WeaponOwner->AddControllerYawInput(FMath::FRandRange(RecoilForce * -1, RecoilForce));

				PC->ClientPlayCameraShake(UseCameraShake);
			}
		}
	}
}

void ASFireWeaponBase::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(ASFireWeaponBase, CurrentAmmoInClip, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(ASFireWeaponBase, TotalAmmo, COND_OwnerOnly);
	DOREPLIFETIME(ASFireWeaponBase, Reloading);
}

