// Fill out your copyright notice in the Description page of Project Settings.

#include "SGameStateBase.h"
#include "Net/UnrealNetwork.h"
#include "SGettersLib.h"
#include "SGameModeBase.h"

ASGameStateBase::ASGameStateBase()
{
	TimeWaitingBeforeStart = 5;
	TimeWaitingAfterEnd = 20;

	GameTime = TimeWaitingBeforeStart;
}

void ASGameStateBase::BeginPlay()
{
	Super::BeginPlay();

	if (Role == ROLE_Authority)
	{
		LimitByTimeFromGameMode = USGettersLib::SGetGameModeBase(this)->GetLimitByTime();
	}
}

void ASGameStateBase::GameStateTick()
{
	Super::GameStateTick();

	switch (GameState)
	{
	case EGameState::Wait:
		TimeWaitingBeforeStart--;
		GameTime = TimeWaitingBeforeStart;
		if (TimeWaitingBeforeStart <= 0)
		{
			SetTime_LimitFromGameMode();
			SetNewGameState(EGameState::Game);
			USGettersLib::SGetGameModeBase(this)->StartGame();
		}
		break;
	case EGameState::Game:
		if (LimitByTimeFromGameMode)
		{
			GameTime--;
			if (GameTime <= 0)
			{
				SetTime_TimeWaitAfterEnd();
				SetNewGameState(EGameState::GameEnd);
				USGettersLib::SGetGameModeBase(this)->TimeLimitReached();
			}
		}
		else
		{
			GameTime++;
		}
		GameLogicTick();
		break;
	case EGameState::GameEnd:
		TimeWaitingAfterEnd--;
		GameTime = TimeWaitingAfterEnd;
		if (TimeWaitingAfterEnd <= 0)
		{
			USGettersLib::SGetGameModeBase(this)->EndGame();
		}
		break;
	default:
		break;
	}
}

void ASGameStateBase::GameLogicTick()
{

}

void ASGameStateBase::NotificationPlayerDead_Implementation(APlayerState* DeadCharacter, APlayerState* DeathInstigator, AActor* DeathCausedBy)
{
	OnPlayerDead.Broadcast(DeadCharacter, DeathInstigator, DeathCausedBy);
}

void ASGameStateBase::SetTime_LimitFromGameMode()
{
	if (LimitByTimeFromGameMode)
	{
		GameTime = USGettersLib::SGetGameModeBase(this)->GetTimeLimit();
	}
	else
	{
		GameTime = 0;
	}
}

void ASGameStateBase::SetTime_TimeWaitAfterEnd()
{
	GameTime = TimeWaitingAfterEnd;
}

void ASGameStateBase::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASGameStateBase, GameTime);
}

