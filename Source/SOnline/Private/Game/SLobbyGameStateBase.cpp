// Fill out your copyright notice in the Description page of Project Settings.

#include "SLobbyGameStateBase.h"
#include "TimerManager.h"
#include "Engine/DataTable.h"
#include "Net/UnrealNetwork.h"
#include "SGettersLib.h"
#include "SGameInstance.h"

ASLobbyGameStateBase::ASLobbyGameStateBase()
{
	PlayersForStartGame = 2;
	TimeToStart = 60;
}

void ASLobbyGameStateBase::BeginPlay()
{
	Super::BeginPlay();

	ExtractMapFromTable();
}

void ASLobbyGameStateBase::GameStateTick() // Called every second
{
	Super::GameStateTick();

	TimeToStart--;

	if (TimeToStart <= 0)
	{
		if (EnoughPlayersToStart())
		{
			StartGame();
		}
		else
		{
			DestroyLobby("Lack of players");
		}
	}
}

void ASLobbyGameStateBase::PlayerReady(APlayerState* PlayerState)
{
	Super::PlayerReady(PlayerState);

	if (EnoughPlayersToStart())
	{
		if (AllPlayersReady())
		{
			StartGame();
		}
	}
}

bool ASLobbyGameStateBase::ExtractMapFromTable()
{
	if (TableMapList)
	{
		TArray<FSGameMap*> MapsFromTable;
		FString ContextString;
		TableMapList->GetAllRows(ContextString, MapsFromTable);

		for (FSGameMap *It : MapsFromTable)
		{
			MapList.Add(*It);
			VotesForMap.Add(0);
		}

		return true;
	}

	return false;
}

FSGameMap ASLobbyGameStateBase::GetFavoriteMap()
{
	int MaxIndexVotes = 0;
	int MapIndex = 0;

	for (int i = 0; i < VotesForMap.Num(); i++)
	{
		if (VotesForMap[i] > MaxIndexVotes)
		{
			MaxIndexVotes = VotesForMap[i];
			MapIndex = i;
		}
	}

	return MapList[MapIndex];
}

void ASLobbyGameStateBase::StartGame()
{
	GetWorld()->ServerTravel("/Game/Maps/" + GetFavoriteMap().RealMapName + "?listen");
}

void ASLobbyGameStateBase::DestroyLobby(FString Reason)
{
	USGettersLib::SGetGameInstance(this)->DestroySessionAndLeaveGame();
}

void ASLobbyGameStateBase::UpdateMapsVotes()
{
	for (int i = 0; i < VotesForMap.Num(); i++) // Reset array
	{
		VotesForMap[i] = 0;
	}

	for (FVote It : Votes)
	{
		VotesForMap[It.MapIndex]++;
	}
}

void ASLobbyGameStateBase::SendVote_Implementation(APlayerController* Player, int MapIndex)
{
	if (!Player)
	{
		return;
	}

	for (int i = 0; i < Votes.Num(); i++)
	{
		if (Votes[i].Player == Player)
		{
			if (Votes[i].MapIndex == MapIndex)
			{
				return;
			}
			else
			{
				Votes[i].MapIndex = MapIndex;
				UpdateMapsVotes();
				return;
			}
		}
	}

	FVote NewVote;
	NewVote.Player = Player;
	NewVote.MapIndex = MapIndex;
	Votes.Add(NewVote);
	UpdateMapsVotes();
}

bool ASLobbyGameStateBase::SendVote_Validate(APlayerController* Player, int MapIndex)
{
	return true;
}

bool ASLobbyGameStateBase::EnoughPlayersToStart()
{
	return PlayerArray.Num() >= PlayersForStartGame;
}

void ASLobbyGameStateBase::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASLobbyGameStateBase, VotesForMap);
	DOREPLIFETIME(ASLobbyGameStateBase, TimeToStart);
}